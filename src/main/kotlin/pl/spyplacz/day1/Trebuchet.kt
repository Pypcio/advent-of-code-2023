package pl.spyplacz.day1

import java.nio.file.Paths

class Trebuchet {

    fun calculateCalibration(fileName: String): Int {
        val resource = this.javaClass.classLoader.getResource(fileName)
        val lines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val firsts = lines.map { findFirstDigit(it, false) }
        val lasts = lines.map { findLastDigit(it, false) }

        val calibration = firsts.sumOf { it*10 } + lasts.sumOf { it }
        for (i in lines.indices) {
//            println(lines[i] + " || " + firsts[i] + lasts[i])
        }
        return calibration
    }

    fun calculateCalibrationWithWordParsing(fileName: String): Int {
        val resource = this.javaClass.classLoader.getResource(fileName)
        val lines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val firsts = lines.map { findFirstDigit(it) }
        val lasts = lines.map { findLastDigit(it) }

        val calibration = firsts.sumOf { it*10 } + lasts.sumOf { it }
        for (i in lines.indices) {
//            println(lines[i] + " || " + firsts[i] + lasts[i])
        }
        return calibration
    }



    private fun findFirstDigit(line: String, withParsing: Boolean = true): Int {
        for (i in line.indices){
            if ( line[i].isDigit() ) return line[i].toString().toInt()
            if (withParsing) {
                val substring = line.substring(i)
                if (substring.startsWith("one")) return 1
                if (substring.startsWith("two")) return 2
                if (substring.startsWith("three")) return 3
                if (substring.startsWith("four")) return 4
                if (substring.startsWith("five")) return 5
                if (substring.startsWith("six")) return 6
                if (substring.startsWith("seven")) return 7
                if (substring.startsWith("eight")) return 8
                if (substring.startsWith("nine")) return 9
            }
        }
        return 0
    }

    private fun findLastDigit(line: String, withParsing: Boolean = true): Int {
        for (i in line.length -1 downTo  0){
            if ( line[i].isDigit() ) return line[i].toString().toInt()
            if (withParsing) {
                val substring = line.substring(i)
                if (substring.startsWith("one")) return 1
                if (substring.startsWith("two")) return 2
                if (substring.startsWith("three")) return 3
                if (substring.startsWith("four")) return 4
                if (substring.startsWith("five")) return 5
                if (substring.startsWith("six")) return 6
                if (substring.startsWith("seven")) return 7
                if (substring.startsWith("eight")) return 8
                if (substring.startsWith("nine")) return 9
            }
        }
        return 0
    }
}