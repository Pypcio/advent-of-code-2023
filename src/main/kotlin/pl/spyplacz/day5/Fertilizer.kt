package pl.spyplacz.day5

import java.nio.file.Paths
import java.util.concurrent.Executors

class Fertilizer {

    companion object {

        const val SEED_TO_SOIL = "seed-to-soil map:"
        const val SOIL_TO_FERTILIZER = "soil-to-fertilizer map:"
        const val FERTILIZER_TO_WATER = "fertilizer-to-water map:"
        const val WATER_TO_LIGHT = "water-to-light map:"
        const val LIGHT_TO_TEMPERATURE = "light-to-temperature map:"
        const val TEMPERATURE_TO_HUMIDITY = "temperature-to-humidity map:"
        const val HUMIDITY_TO_LOCATION = "humidity-to-location map:"

        val mapList = listOf(
                SEED_TO_SOIL,
                SOIL_TO_FERTILIZER,
                FERTILIZER_TO_WATER,
                WATER_TO_LIGHT,
                LIGHT_TO_TEMPERATURE,
                TEMPERATURE_TO_HUMIDITY,
                HUMIDITY_TO_LOCATION
        )

        class SourceToDestinationMap(input: String) {

            private val sourceToDests = input.split(" ").filter { it.isNotEmpty() }.map { it.toLong() }

            val destinationRange = sourceToDests[0]
            val sourceRange = sourceToDests[1]
            val length = sourceToDests[2]
            val totalRange = sourceRange + length - 1
            val range = Pair(sourceRange, sourceRange + length)

            fun isInRange(position: Long): Boolean {
                val validation = (sourceRange <= position && position < sourceRange + length)
                return validation
            }

            fun getValue(position: Long): Long {
                val value = position - sourceRange + destinationRange
                return value
            }

            override fun toString(): String {
                return "destination range: $destinationRange, source range: $sourceRange, length: $length"
            }

        }

    }

    fun getLowestLocation(fileName: String): Long {
        val resource = this.javaClass.classLoader.getResource(fileName)
        val lines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val seeds = lines.first().split(" ").drop(1).map { it.toLong() }

        val mapList = mapList.map { arg ->
            var i = lines.indexOf(arg) + 1
            val categoryList = mutableListOf<SourceToDestinationMap>()
            while (lines[i].isNotEmpty() && i < lines.size - 1) {
                val sourceToDest = SourceToDestinationMap(lines[i])
                categoryList.add(sourceToDest)
                i++
            }
            categoryList.sortBy { it.sourceRange }
            mapOf(arg to categoryList)
        }
                .flatMap { it.entries }
                .associate { it.key to it.value }

        var min = Long.MAX_VALUE

        for (i in seeds.indices) {
            val soil = getSingleLocation(seeds[i], mapList[SEED_TO_SOIL]!!)
            val fertilizer = getSingleLocation(soil, mapList[SOIL_TO_FERTILIZER]!!)
            val water = getSingleLocation(fertilizer, mapList[FERTILIZER_TO_WATER]!!)
            val light = getSingleLocation(water, mapList[WATER_TO_LIGHT]!!)
            val temperature = getSingleLocation(light, mapList[LIGHT_TO_TEMPERATURE]!!)
            val humidity = getSingleLocation(temperature, mapList[TEMPERATURE_TO_HUMIDITY]!!)
            val location = getSingleLocation(humidity, mapList[HUMIDITY_TO_LOCATION]!!)
            if (location < min) min = location
        }
        return min
    }

    fun getLowestLocation2(fileName: String): Long {
        val resource = this.javaClass.classLoader.getResource(fileName)
        val lines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val seeds = lines.first().split(" ").drop(1).map { it.toLong() }
        val seedsPair = seeds.chunked(2)
        val seedsRanges = seedsPair.map { Pair(it[0], it[0] + it[1]) }

        val mapList = mapList.map { arg ->
            var i = lines.indexOf(arg) + 1
            val categoryList = mutableListOf<SourceToDestinationMap>()
            while (lines[i].isNotEmpty() && i < lines.size - 1) {
                val sourceToDest = SourceToDestinationMap(lines[i])
                categoryList.add(sourceToDest)
                i++
            }
            categoryList.sortBy { it.sourceRange }
            mapOf(arg to categoryList)
        }
                .flatMap { it.entries }
                .associate { it.key to it.value }

        val executorThread = Executors.newFixedThreadPool(seedsRanges.size)

        val seedsMin = seedsRanges.map {
            executorThread.run { calculateSeed(it, mapList) }
        }
        println(seedsMin)
        return seedsMin.minOf { it }
    }


    private fun calculateSeed(it: Pair<Long, Long>, mapList: Map<String, MutableList<SourceToDestinationMap>>) : Long {
        var min = Long.MAX_VALUE
        for (i in it.first until it.second) {
            val soil = getSingleLocation(i, mapList[SEED_TO_SOIL]!!)
            val fertilizer = getSingleLocation(soil, mapList[SOIL_TO_FERTILIZER]!!)
            val water = getSingleLocation(fertilizer, mapList[FERTILIZER_TO_WATER]!!)
            val light = getSingleLocation(water, mapList[WATER_TO_LIGHT]!!)
            val temperature = getSingleLocation(light, mapList[LIGHT_TO_TEMPERATURE]!!)
            val humidity = getSingleLocation(temperature, mapList[TEMPERATURE_TO_HUMIDITY]!!)
            val location = getSingleLocation(humidity, mapList[HUMIDITY_TO_LOCATION]!!)
            if (location < min) min = location
        }
        println("current min: $min")
        return min
    }

    private fun getSingleLocation(arg: Long, sourceToDestinationMap: MutableList<SourceToDestinationMap>): Long {
        val isInRange = sourceToDestinationMap.filter { it.isInRange(arg) }
        return if (isInRange.isNotEmpty()) isInRange.first().getValue(arg)
        else arg
    }

}