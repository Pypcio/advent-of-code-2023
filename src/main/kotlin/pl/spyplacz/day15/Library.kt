package pl.spyplacz.day15

import java.nio.file.Paths

class Library {

    companion object {

        const val MULTIPLIED = 17
        const val REMAINDER = 256

        class Hash(val hashString: String) {
            fun calculateValue(): Long {
                var currentValue = 0L
                for (i in hashString.indices) {
                    currentValue += hashString[i].code
                    currentValue *= MULTIPLIED
                    currentValue %= REMAINDER
                }
                println("final value for $hashString: $currentValue")
                return currentValue
            }
        }

        class HashForBoxex(val hashString: String) {
            val sign = if (hashString.contains('=')) '=' else '-'
            val lens = if (sign == '=') hashString.split('=').first() else hashString.split('-').first()
            val focal : Int = if (sign == '=') hashString.split('=').last().toInt() else 0

            val box = calculateValue()

            override fun toString(): String {
                return "sign $sign, lens $lens, focal $focal, box: $box\n"
            }


            private fun calculateValue(): Int {
                var currentValue = 0
                for (i in lens.indices) {
                    currentValue += lens[i].code
                    currentValue *= MULTIPLIED
                    currentValue %= REMAINDER
                }
                return currentValue
            }

        }

    }


    fun hash(filename: String): Long {
        val resource = this.javaClass.classLoader.getResource(filename)
        val hashesLine = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }.first()
        val hashes = hashesLine.split(",").map { Hash(it) }

        println(hashes)
        val hashesValue = hashes.map { it.calculateValue()}
        return hashesValue.sum()
    }

    fun hashPower(filename: String): Long {
        val resource = this.javaClass.classLoader.getResource(filename)
        val hashesLine = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }.first()
        val hashes = hashesLine.split(",").map { HashForBoxex(it) }

        val boxes = ArrayList<MutableMap<String, Int>>(256)
        for (i in 0..255){
            val mutableMap = LinkedHashMap<String, Int>()
            boxes.add(mutableMap)
        }

        for (i in hashes.indices){
            val currentHash = hashes[i]
            if (currentHash.sign == '='){
                val mutableMap = boxes[currentHash.box]
                mutableMap[currentHash.lens] = currentHash.focal
            } else {
                // remove
                val mutableMaps = boxes[currentHash.box]
                if (mutableMaps.contains(currentHash.lens)) {
                    mutableMaps.remove(currentHash.lens)
                }
            }
        }

        println(boxes)

        var currentSum = 0L
        for (i in boxes.indices) {
            var boxSum = 0
            val boxFactor = i + 1
            val list = boxes[i].entries.toList()
            for (l in list.indices) {
                val lineFactor = l + 1
                boxSum += lineFactor * boxFactor * list[l].value
            }
            currentSum += boxSum
        }
        return currentSum
    }

}