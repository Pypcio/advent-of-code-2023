package pl.spyplacz.day9

import java.nio.file.Paths

class Mirage {

    companion object {

        class Sensor(val mainLine: List<Int>) {

            val allLines = calculateAllLines(mainLine)

            fun getHistory(): Long {
                var currentHistory = allLines.last().last().toLong()
                for (i in allLines.size - 2 downTo 0) {
                    val lineHistory = allLines[i].last()
                    currentHistory += lineHistory
                }
                return currentHistory
            }

            fun getBackwardsHistory(): Long {
                var currentHistory = allLines.last().first().toLong()
                for (i in allLines.size - 2 downTo 0) {
                    val lineHistory = allLines[i].first()
                    currentHistory = lineHistory - currentHistory
                }
                return currentHistory
            }

            private fun calculateAllLines(mainLine: List<Int>): List<List<Int>> {
                val finalList: MutableList<List<Int>> = mutableListOf(mainLine)
                var currentLine = mainLine
                while (currentLine.sum() != 0) {
                    val list = buildNextHistoryLine(currentLine)
                    currentLine = list
                    finalList.add(list)
                }
                return finalList
            }

            private fun buildNextHistoryLine(previousList: List<Int>): List<Int> {
                val newList = mutableListOf<Int>()
                for (i in previousList.size - 1 downTo 1) {
                    val diff = previousList[i] - previousList[i - 1]
                    newList.add(0, diff)
                }
                return newList
            }
        }
    }

    fun sumExtrapolatedValues(filename: String): Long {
        val resource = this.javaClass.classLoader.getResource(filename)
        val textLines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val numberLines = textLines.map { it.split(" ").map { line -> line.toInt() } }
        val sensors = numberLines.map { Sensor(it) }
        val histories = sensors.map { it.getHistory() }
        return histories.sum()
    }

    fun sumBackwardsExtrapolatedValues(filename: String): Long {
        val resource = this.javaClass.classLoader.getResource(filename)
        val textLines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val numberLines = textLines.map { it.split(" ").map { line -> line.toInt() } }
        val sensors = numberLines.map { Sensor(it) }
        val histories = sensors.map { it.getBackwardsHistory() }
        return histories.sum()
    }
}