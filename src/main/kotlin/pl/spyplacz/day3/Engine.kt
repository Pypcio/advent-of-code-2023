package pl.spyplacz.day3

import java.nio.file.Paths

class Engine {

    companion object {

        class Point(val x: Int, val y: Int) {

            override fun toString(): String{
                return "x: $x y: $y";
            }
        }

        class Symbol(val location: Point, val char: Char) {

            fun findSymbolNumbers(allNumbers: List<Number>) : List<Number> {
                return allNumbers.filter { it.validate(this) }
            }

            override fun toString(): String{
                return "x: ${location.x} y: ${location.y}";
            }
        }

        class Number(val location: Point, val value: Int) {

            override fun toString(): String{
                return "x: ${location.x} y: ${location.y}, value $value"
            }

            fun validate(symbols: List<Symbol>) : Boolean{
                val validations = symbols.map { x -> validate(x) }
                return validations.any { x -> x }
            }


            fun length(): Int {
                return value.toString().length
            }

            fun validate(symbol: Symbol) : Boolean{

                val symbolXRange = (symbol.location.x-1..symbol.location.x+1).toList()
                val numberXRange = (location.x until location.x+length()).toList()
                val xValidate = symbolXRange.toSet().any { it in numberXRange.toSet() }

                val yValidate = symbol.location.y -1 <= location.y && symbol.location.y + 1 >= location.y
                return xValidate && yValidate
            }

        }

    }

    fun engineSchematic(fileName: String): Int {
        val resource = this.javaClass.classLoader.getResource(fileName)
        val lines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val xSize = lines.first().length
        val ySize = lines.size

        val numbers = mutableListOf<Number>()
        val symbols = mutableListOf<Symbol>()

        val sb = StringBuffer()
        for (y in 0 until ySize){
            for (x in 0 until xSize) {
                val char = lines[y][x]
                if (char == '.') {
                    if (sb.isNotEmpty()) {
                        val numberString = sb.toString()
                        val numberX = x-numberString.length
                        val number = Number(Point(numberX, y), numberString.toInt())
                        numbers.add(number)
                        sb.setLength(0)
                    }
                } else if (char.isDigit()) {
                    sb.append(char)
                    if (x == xSize - 1) {
                        val numberString = sb.toString()
                        val numberX = x-numberString.length + 1
                        val number = Number(Point(numberX, y), numberString.toInt())
                        numbers.add(number)
                        sb.setLength(0)
                    }
                } else {
                    if (sb.isNotEmpty()) {
                        val numberString = sb.toString()
                        val numberX = x-numberString.length
                        val number = Number(Point(numberX, y), numberString.toInt())
                        numbers.add(number)
                        sb.setLength(0)
                    }
                    val symbol = Symbol(Point(x,y), lines[y][x])
                    symbols.add(symbol)
                }
            }
        }

        val validateNumbers = numbers.filter { it.validate(symbols) }
        val notValidateNumbers = numbers.filter { !it.validate(symbols) }

        println("valid: ")
        for (i in 0 until ySize) {
            val filter = validateNumbers.filter { x -> x.location.y == i }
            println("${i+1}: ${filter.map{it.value}}")
        }

        println("not valid: ")
        for (i in 0 until ySize) {
            val filter = notValidateNumbers.filter { x -> x.location.y == i }
            println("${i+1}: ${filter.map{it.value}}")
        }
        val valid = validateNumbers.sumOf { it.value }
        return valid
    }

    fun gearRatios(fileName: String) : Int{
        val resource = this.javaClass.classLoader.getResource(fileName)
        val lines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val xSize = lines.first().length
        val ySize = lines.size

        val numbers = mutableListOf<Number>()
        val symbols = mutableListOf<Symbol>()

        val sb = StringBuffer()
        for (y in 0 until ySize){
            for (x in 0 until xSize) {
                val char = lines[y][x]
                if (char == '.') {
                    if (sb.isNotEmpty()) {
                        val numberString = sb.toString()
                        val numberX = x-numberString.length
                        val number = Number(Point(numberX, y), numberString.toInt())
                        numbers.add(number)
                        sb.setLength(0)
                    }
                } else if (char.isDigit()) {
                    sb.append(char)
                    if (x == xSize - 1) {
                        val numberString = sb.toString()
                        val numberX = x-numberString.length + 1
                        val number = Number(Point(numberX, y), numberString.toInt())
                        numbers.add(number)
                        sb.setLength(0)
                    }
                } else {
                    if (sb.isNotEmpty()) {
                        val numberString = sb.toString()
                        val numberX = x-numberString.length
                        val number = Number(Point(numberX, y), numberString.toInt())
                        numbers.add(number)
                        sb.setLength(0)
                    }
                    val symbol = Symbol(Point(x,y), lines[y][x])
                    symbols.add(symbol)
                }
            }
        }

        val gears = symbols.filter { it.char == '*' }
        println("gears: ${gears.map { it.location }}")
        val gearsNumbers = gears.map { mapOf(it to it.findSymbolNumbers(numbers))}.flatMap { it.entries }
            .associate { it.key to it.value  }

        gearsNumbers.forEach { x ->
            run {
                println("${x.key} numbers ${x.value}")
            }
        }

        val gearRatios = gearsNumbers
            .filter { it.value.size == 2 }
            .map { it.value[0].value * it.value[1].value}
            .sum()

        return gearRatios
    }
}