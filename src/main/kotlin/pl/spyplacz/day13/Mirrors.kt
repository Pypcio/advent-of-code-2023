package pl.spyplacz.day13

import java.lang.Integer.min
import java.nio.file.Paths
import java.util.*
import kotlin.collections.ArrayList

class Mirrors {

    companion object {

        class Matrix(val textMatrix: ArrayList<String>){

            fun findMiddleVertical() : Optional<Int> {
                val possibleVertical = textMatrix.map { findMiddleLine(it) }
                val middleOfVertical = getMiddleOfLine(possibleVertical)
                return middleOfVertical
            }

            fun getMiddleOfLine(possibleVertical: List<List<Int>>): Optional<Int> {
                val posVerts = possibleVertical.flatten().toSet().toList()
                for ( i in posVerts.indices){
                    val isInAll = possibleVertical.filter { it.contains(posVerts[i]) }.size == possibleVertical.size
                    if (isInAll) return Optional.of(posVerts[i])
                }
                return Optional.empty()
            }
            private fun findMiddleLine(line: String):List<Int>{
                val possible = ArrayList<Int>()
                // without skipping
                for (i in 1..line.length-1){
                    val first = line.substring(0, i).reversed()
                    val second = line.substring(i)
                    val range = min(first.length, second.length)
                    if (first.substring(0,range) == second.substring(0,range))  possible.add(i)
                }
                return possible.toSet().toList()
            }


            fun findMiddleHorizontal() : Optional<Int> {
                val rows = ArrayList<String>()
                for (i in 0 until textMatrix.first().length) {
                    val row = String(textMatrix.map { it[i] }.toCharArray())
                    rows.add(row)
                }

                val possibleHorizontal = rows.map { findMiddleLine(it) }
                val middleOfHorizontal = getMiddleOfLine(possibleHorizontal)

                return middleOfHorizontal
            }

        }
    }
    fun calculateNotes(filename: String): Long{
        val resource = this.javaClass.classLoader.getResource(filename)
        val matrixesText = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }

        val matrixList = ArrayList<Matrix>()
        var tempMatrix : ArrayList<String> = ArrayList()
        for (i in matrixesText.indices) {
            if (matrixesText[i].isNotEmpty()){
                tempMatrix.add(matrixesText[i])
            } else {
                val matrix = Matrix(tempMatrix)
                matrixList.add(matrix)
                tempMatrix = ArrayList()
            }
        }
        val matrix = Matrix(tempMatrix)
        matrixList.add(matrix)

        var sum = 0L

        for (i in matrixList.indices) {
            println("matrix $i")
            matrixList[i].findMiddleVertical().ifPresent { value -> run {
                println("vertical: $value")
                sum += value.toLong()
            } }
            matrixList[i].findMiddleHorizontal().ifPresent { value -> run {
                println("horizontal: $value")
                sum += value.toLong() * 100
            } }
        }

        return sum
    }
}