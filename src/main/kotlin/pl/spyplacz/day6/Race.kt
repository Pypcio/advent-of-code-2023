package pl.spyplacz.day6

import java.nio.file.Paths

class Race {

    fun waysBeatingRecords(filename: String): Int {
        val resource = this.javaClass.classLoader.getResource(filename)
        val textLines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val times = textLines.first().split(":").last().split(" ").filter { it.isNotEmpty() }.map { it.toInt() }
        val distances = textLines.last().split(":").last().split(" ").filter { it.isNotEmpty() }.map { it.toInt() }
        val timeToDistanceMap = times.zip(distances).toMap()
        val recordsMap = timeToDistanceMap.map { calculateRecords(it) }
        val waysOfRecordsMap = recordsMap.map { it.keys }
        val waysOfRecordsSize = waysOfRecordsMap.map { it.size }
        val waysOfRecords = waysOfRecordsSize.map { it }.reduce { i, j -> i * j }
        return waysOfRecords
    }

    fun waysBeatingRace(filename: String): Long {
        val resource = this.javaClass.classLoader.getResource(filename)
        val textLines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val time = textLines.first().split(":").last().replace(" ", "").toLong()
        val distance = textLines.last().split(":").last().replace(" ", "").toLong()
        val timeToDistance = Pair(time, distance)
        val records = calculateRecords(timeToDistance)
        return records
    }

    private fun calculateRecords(timeToDistanceMap: Map.Entry<Int, Int>): Map<Int, Int> {
        val resultMap: MutableMap<Int, Int> = HashMap()
        val time = timeToDistanceMap.key
        val recordDistance = timeToDistanceMap.value
        for (i in 1 until time) {
            val doneDistance = (time - i) * i
            if (doneDistance > recordDistance) resultMap[i] = doneDistance
        }
        return resultMap.toMap()
    }

    private fun calculateRecords(timeToDistanceMap: Pair<Long, Long>): Long {
        var recordInc = 0L
        val time = timeToDistanceMap.first
        val recordDistance = timeToDistanceMap.second
        for (i in 1 until time) {
            val doneDistance = (time - i) * i
            if (doneDistance > recordDistance) recordInc += 1
        }
        return recordInc
    }
}