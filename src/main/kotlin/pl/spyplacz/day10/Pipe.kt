package pl.spyplacz.day10

import java.nio.file.Paths

class Pipe {

    companion object {

        class Point(val x: Int, val y: Int) {

            override fun toString(): String {
                return "x: $x y: $y";
            }
        }

        class Tile(val char: Char, val point: Point)


    }

    fun calculateSteps(filename: String): Int {
        val resource = this.javaClass.classLoader.getResource(filename)
        val matrixText = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }

        val maxX = matrixText.first().length
        val maxY = matrixText.size

        val newMatrix: Array<Array<Int>> = Array(maxY) { Array(maxX) { -1 } }
        val startingPointY = calculateStartingY(matrixText)
        val startingPointX = matrixText[startingPointY].indexOf("S")
        val startingPoint = Point(startingPointX, startingPointY)

        var pointToEvaluate = ArrayList<Point>()
        var currentValue = -1
        pointToEvaluate.add(startingPoint)
        newMatrix[startingPointY][startingPointX] = 0

        while (pointToEvaluate.isNotEmpty()) {
            currentValue += 1
            val lists = pointToEvaluate
                    .map { markNextTile(matrixText, newMatrix, it, currentValue) }
                    .flatten()
            pointToEvaluate.clear()
            pointToEvaluate.addAll(lists)
//            printMatrix(currentValue, newMatrix)
        }

        printMatrix(newMatrix, currentValue)
        return currentValue
    }

    fun calculateEnclosed(filename: String): Int {
        val resource = this.javaClass.classLoader.getResource(filename)
        val matrixText = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }

        val maxX = matrixText.first().length
        val maxY = matrixText.size

        val newMatrix: Array<Array<Int>> = Array(maxY) { Array(maxX) { -1 } }
        val startingPointY = calculateStartingY(matrixText)
        val startingPointX = matrixText[startingPointY].indexOf("S")
        val startingPoint = Point(startingPointX, startingPointY)

        var pointToEvaluate = ArrayList<Point>()
        var currentValue = -1
        pointToEvaluate.add(startingPoint)
        newMatrix[startingPointY][startingPointX] = 0

        while (pointToEvaluate.isNotEmpty()) {
            currentValue += 1
            val lists = pointToEvaluate
                    .map { markNextTile(matrixText, newMatrix, it, currentValue) }
                    .flatten()
            pointToEvaluate.clear()
            pointToEvaluate.addAll(lists)
//            printMatrix(currentValue, newMatrix)
        }

        printMatrix(newMatrix, currentValue)
        val replaceStart = replaceStart(newMatrix, startingPoint)
        val updatedMatrixText = ArrayList<String>()
        for (i in matrixText.indices){
            updatedMatrixText.add(matrixText[i].replace('S', replaceStart))
        }
        val enclosingMatrix = calculateEnclosingMatrix(updatedMatrixText, newMatrix)

        printBoolMatrix(enclosingMatrix)

        var sum = 0
        for (i in 0 until maxY) {
            for ( j in 0 until maxX) {
                if (enclosingMatrix[i][j] && newMatrix[i][j] == -1) {
                    println("$j, $i")
                    sum +=1
                }
            }
        }

        return sum
    }

    private fun calculateEnclosingMatrix(matrixText: List<String>, calculatedMatrix: Array<Array<Int>>): Array<Array<Boolean>> {
        val newMatrix: Array<Array<Boolean>> = Array(matrixText.size) { Array(matrixText.first().length) { false } }
        // if even crossing the line in all directions -> is not inside the loop
        for (i in 1..matrixText.size-2) {
            for (j in 1 until matrixText.first().length - 1) {
                val checkedPosition = newMatrix[i][j]
                val line = matrixText[i]
                val lineLoopContains = calculatedMatrix[i].map { it > -1 }

                val leftPart = subLineFrom(line, j, lineLoopContains)
                val rightPart = subLineTo(line, j+1, lineLoopContains)
                val leftCheck = horizontalCheck(leftPart)
                val rightCheck = horizontalCheck(rightPart)

                val verticalLine = String(matrixText.map { it[j] }.toCharArray())
                val verticalLineContain = calculatedMatrix.map { it[j] > -1 }

                val topPart = subLineFrom(verticalLine, i, verticalLineContain)
                val botPart = subLineTo(verticalLine, i+1, verticalLineContain)
                val topCheck = verticalCheck(topPart)
                val botCheck = verticalCheck(botPart)

                newMatrix[i][j] = leftCheck % 2 != 0 && rightCheck % 2 != 0 && topCheck % 2 != 0 && botCheck % 2 != 0
            }
        }
        return newMatrix
    }


    private fun subLineFrom(line: String, i: Int, contain: List<Boolean>) : String {
        val sb = StringBuilder()
        for (i in 0 until i) {
            if (contain[i]) sb.append(line[i])
        }
        return sb.toString()
    }

    private fun subLineTo(line: String, i: Int, contain: List<Boolean>) : String {
        val sb = StringBuilder()
        for (i in i until line.length) {
            if (contain[i]) sb.append(line[i])
        }
        return sb.toString()
    }

    private fun calculateVertCorrectLine(line: String, loopLine: List<Int>) : String {
        val sb = StringBuilder()
        for (i in 0 until line.length) {
            if (loopLine[i] > -1) sb.append(line[i])
        }
        return sb.toString()

    }
    private fun horizontalCheck(line: String) : Int {
        val replaced = line.replace("-", "").replace(".", "")
        var sum = replaced.count{it == '|'}
        val replaced2 = replaced.replace("|", "")
        val chunked = replaced2.chunked(2)
        val map = chunked.map { horizontalReplace(it) }
        return map.sum() + sum
    }

    private fun horizontalReplace(line: String) : Int {
        if (line.length == 1) return 1
        else {
            if (line == "L7") return 1
            if (line == "FJ") return 1
        }
        return 0
    }

    private fun verticalCheck(line: String) : Int {
        val replaced = line.replace("|", "").replace(".", "")
        var sum = replaced.count{it == '-'}
        val replaced2 = replaced.replace("-", "")
        val chunked = replaced2.chunked(2)
        val map = chunked.map { verticalReplace(it) }
        return map.sum() + sum
    }

    private fun verticalReplace(line: String): Int{
        if (line.length == 1) return 1
        else {
            if (line == "7L") return 1
            if (line == "FJ") return 1
            if (line == "JF") return 1
        }
        return 0
    }
    private fun replaceStart(newMatrix: Array<Array<Int>>, startPoint: Point) : Char {
        val top = if(startPoint.y > 0) newMatrix[startPoint.y-1][startPoint.x] else 0
        val bot = newMatrix[startPoint.y+1][startPoint.x]
        val left = newMatrix[startPoint.y][startPoint.x-1]
        val right = newMatrix[startPoint.y][startPoint.x+1]
        if (top == 1) {
            if (bot == 1) return '|'
            if (left == 1) return 'J'
            if (right == 1) return 'L'
        }
        if (bot == 1) {
            if (left == 1) return '7'
            if (right == 1) return 'F'
        }
        if (left == 1 && right == 1) return '-'
        return '.'
    }

    private fun printMatrix(newMatrix: Array<Array<Int>>, currentValue: Int = 0) {
        //printing
        println("---------------------------------------------")
        println("STEP: $currentValue")
        println()

        for (j in newMatrix.indices) {
            for (i in newMatrix.first().indices) {
                val str = if (newMatrix[j][i] == -1) " X" else if (newMatrix[j][i] < 10 && newMatrix[j][i] > -1) " ${newMatrix[j][i]}" else newMatrix[j][i]
                print(" $str ")
            }
            println()
        }

        println("---------------------------------------------")
        //printing
    }

    private fun printBoolMatrix(newMatrix: Array<Array<Boolean>>, currentValue: Int = 0) {
        //printing
        println("---------------------------------------------")
        println("STEP: $currentValue")
        println()

        for (j in newMatrix.indices) {
            for (i in newMatrix.first().indices) {
                val str = if (newMatrix[j][i]) "X" else "."
                print(" $str ")
            }
            println()
        }

        println("---------------------------------------------")
        //printing
    }

    private fun calculateStartingY(matrix: List<String>): Int {
        for (i in matrix.indices) {
            if (matrix[i].contains("S")) return i
        }
        return -1
    }

    private fun markNextTile(matrix: List<String>, newMatrix: Array<Array<Int>>, currentPoint: Point, value: Int): ArrayList<Point> {

        val currentChar = matrix[currentPoint.y][currentPoint.x]
        val newPoints = ArrayList<Point>()
        val newValue = value + 1

        if (currentPoint.y > 0) {
            val newPoint = Point(currentPoint.x, currentPoint.y - 1)
            val topTile = matrix[newPoint.y][newPoint.x]
            if ((topTile == '|' || topTile == '7' || topTile == 'F') && (currentChar == '|' || currentChar == 'J' || currentChar == 'L' || currentChar == 'S')) {
                if (newMatrix[newPoint.y][newPoint.x] == -1) {
                    newMatrix[newPoint.y][newPoint.x] = newValue
                    newPoints.add(newPoint)
                }
            }
        }

        if (currentPoint.y < matrix.size - 1) {
            val newPoint = Point(currentPoint.x, currentPoint.y + 1)
            val bottomTile = matrix[newPoint.y][newPoint.x]
            if ((bottomTile == '|' || bottomTile == 'L' || bottomTile == 'J') && (currentChar == '|' || currentChar == '7' || currentChar == 'F' || currentChar == 'S')) {
                if (newMatrix[newPoint.y][newPoint.x] == -1) {
                    newMatrix[newPoint.y][newPoint.x] = newValue
                    newPoints.add(newPoint)
                }
            }
        }

        if (currentPoint.x > 0) {
            val newPoint = Point(currentPoint.x - 1, currentPoint.y)
            val leftTile = matrix[newPoint.y][newPoint.x]
            if ((leftTile == '-' || leftTile == 'L' || leftTile == 'F') && (currentChar == '-' || currentChar == '7' || currentChar == 'J' || currentChar == 'S')) {
                if (newMatrix[newPoint.y][newPoint.x] == -1) {
                    newMatrix[newPoint.y][newPoint.x] = newValue
                    newPoints.add(newPoint)
                }
            }
        }


        if (currentPoint.x < matrix.first().length - 1) {
            val newPoint = Point(currentPoint.x + 1, currentPoint.y)
            val rightTile = matrix[newPoint.y][newPoint.x]
            if ((rightTile == '-' || rightTile == 'J' || rightTile == '7') && (currentChar == '-' || currentChar == 'F' || currentChar == 'L' || currentChar == 'S')) {
                if (newMatrix[newPoint.y][newPoint.x] == -1) {
                    newMatrix[newPoint.y][newPoint.x] = newValue
                    newPoints.add(newPoint)
                }
            }
        }
        return newPoints
    }

}