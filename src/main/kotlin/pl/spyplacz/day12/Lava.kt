package pl.spyplacz.day12

import java.nio.file.Paths
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.concurrent.Executors
import java.util.stream.Collectors
import java.util.stream.IntStream
import kotlin.math.min

class Lava {

    companion object {

        class Line(val arrangements: String, val groups: List<Int>, fold: Boolean = false) {

            fun calculateAvailableArrangements(): Int {
                val numbersOfUnknown = arrangements.count { it == '?' }
                if (numbersOfUnknown == 0) return 1
                else {
//                    println("Line: ")
                    var sumOfArrangements = 0
                    val possibilityArray = bool(numbersOfUnknown)
                    val listOfUnknowns = listOfUnknowns(arrangements)
                    for (i in possibilityArray.indices) {
                        var newLine = arrangements
                        for (j in listOfUnknowns.indices) {
                            val positionInLine = listOfUnknowns[j]
                            val currentPosition = possibilityArray[i][j]
                            val currentChar = if (currentPosition) '#' else '.'
                            newLine = newLine.replaceRange(positionInLine, positionInLine + 1, currentChar.toString())
                        }
                        if (satisfy(newLine, groups)) sumOfArrangements += 1

                    }
//                    println("End line, sum of arrangements: $sumOfArrangements")
                    return sumOfArrangements
                }
            }

            fun calculateAvailableArrangements2(): Long {
                val newGroups = groups + groups
                val numbersOfUnknown = arrangements.count { it == '?' }
                val numbersOfLava = arrangements.count { it == '#' }
                val numbersOfBreak = arrangements.count { it == '.' }
                if (numbersOfUnknown == 0) return 1
                else {
//                    println("1 ${LocalDateTime.now()}")
                    println("Line: $arrangements")
                    var sumOfArrangements = 0L
                    val possibilityArray = bool(numbersOfUnknown)
//                    val possibilityArray = bool(numbersOfUnknown*2+1, newGroups.sum() + numbersOfLava, newGroups.size - 2)
                    val listOfUnknowns = listOfUnknowns(arrangements)
                    val lines = ArrayList<String>()
                    lines.addAll(getSubline(possibilityArray, listOfUnknowns, newGroups.sum()))
//                    println("2 ${LocalDateTime.now()}")
                    val chunked = lines.indices.chunked(8)
                    val executorThread = Executors.newFixedThreadPool(chunked.size)

                    for (i in chunked.indices){
                        sumOfArrangements += executorThread.run { calculatePartialSum(lines, newGroups, chunked[i]) }
                    }
//                    println("3 ${LocalDateTime.now()}")

//                    println("End line, sum of arrangements: $sumOfArrangements")
                    return sumOfArrangements
                }
            }


            private fun calculatePartialSum(lines: ArrayList<String>, newGroups: List<Int>, chunk: List<Int>): Int{
                var sumOfArrangements = 0
                for (i in chunk) {
                    val firstPart = lines[i]
                    val missingLava = newGroups.sum() - firstPart.count { it == '#' }
                    val filteredLines = lines.filter { it.count { it == '#' } == missingLava}
                    val filteredLinesWithJoker = lines.filter { it.count { it == '#' } == missingLava -1 }
                    for (j in filteredLines.indices) {
                        val sign = "."
                        val newLine = firstPart + sign + filteredLines[j]
                        val groupedLine = newLine.split(".").filter { it.isNotEmpty() }.map { it.length }
                        if (groupedLine == newGroups) sumOfArrangements += 1
                    }
                    for (j in filteredLinesWithJoker.indices) {
                        val sign = '#'
                        val newLine = firstPart + sign + filteredLinesWithJoker[j]
                        val groupedLine = newLine.split(".").filter { it.isNotEmpty() }.map { it.length }
                        if (groupedLine == newGroups) sumOfArrangements += 1
                    }
                }
                return sumOfArrangements
            }
            private fun satisfy(newLine: String, groups: List<Int>): Boolean {
                var simplifiedLine = newLine
                while (simplifiedLine.contains("..")) {
                    simplifiedLine = simplifiedLine.replace("..", ".")
                }
                val listOfArrangements = simplifiedLine.split(".").filter { it.isNotEmpty() }
                if (listOfArrangements.size != groups.size) return false
                else {
                    for (i in listOfArrangements.indices) {
                        if (listOfArrangements[i].length != groups[i]) return false
                    }
                    return true
                }
            }

            private fun getSubline(possibilityArray: List<BooleanArray>, listOfUnknowns: ArrayList<Int>, maxLava: Int): Set<String> {
                var newLine = arrangements
                val list = HashSet<String>()
                for (i in possibilityArray.indices) {
                    for (j in listOfUnknowns.indices) {
                        val positionInLine = listOfUnknowns[j]
                        val currentPosition = possibilityArray[i][j]
                        val currentChar = if (currentPosition) '#' else '.'
                        newLine = newLine.replaceRange(positionInLine, positionInLine + 1, currentChar.toString())
                                .replace("?", ".")
                        if (newLine.count{it == '#'} <= maxLava) list.add(newLine)
                    }
                }
                return list
            }

            private fun listOfUnknowns(currentArrangements: String): ArrayList<Int> {
                val listOfUnknowns = ArrayList<Int>()
                for (i in currentArrangements.indices) {
                    if (currentArrangements[i] == '?') listOfUnknowns.add(i)
                }
                return listOfUnknowns
            }

            private fun bool(n: Int): List<BooleanArray> {
                return IntStream.range(0, 1 shl n).mapToObj { i: Int ->
                    val ba = BooleanArray(n)
                    for (j in 0 until n) ba[j] = i ushr j and 1 != 0
                    ba
                }.collect(Collectors.toList())
            }

            private fun bool(n: Int, maxTrue: Int, minFalse: Int): List<BooleanArray> {
                val array = ArrayList<BooleanArray>()
                val max = 1 shl n
                for (i in 0 until max){
                    val ba = BooleanArray(n)
                    for (j in 0 until n) ba[j] = i ushr j and 1 != 0
                    if (ba.count { true } <= maxTrue && ba.count { false } >= minFalse)
                        array.add(ba)
                }
                return array.toList()
            }

        }
    }

    fun calculateFoldAvailableOptions(filename: String): Long {
        val useFactors = 5 - 1
        val resource = this.javaClass.classLoader.getResource(filename)
        val matrixText = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val lines = matrixText.map { line ->
            Line(
                    line.split(" ").first(),
                    line.split(" ").last().split(",").map { it.toInt() })
        }
        val base2 = lines.map { it.calculateAvailableArrangements2() }
        val base = lines.map { it.calculateAvailableArrangements() }

        var sum = 0L

        for (i in base.indices) {
            val factor = base2[i] / base[i]
            val currentArrangement = base[i] * factor * factor * factor * factor
            sum += currentArrangement
        }
        return sum
    }


}

