package pl.spyplacz.day11

import java.nio.file.Paths

class Galaxies {

    companion object{

        const val GALAXY = '#'
        class Galaxy(val x: Int, val y: Int) {

            override fun toString(): String {
                return "x: $x y: $y";
            }
        }

    }
    fun calculateDistancesSum(filename: String) : Long{
        val resource = this.javaClass.classLoader.getResource(filename)
        val matrixText = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val galaxies = ArrayList<Galaxy>()

        val maxX = matrixText.first().length
        val maxY = matrixText.size

        for (i in 0 until maxY){
            for (j in 0 until maxX) {
                if (matrixText[j][i] == GALAXY) {
                    galaxies.add(Galaxy(i, j))
                }
            }
        }

        println(galaxies)


        val emptyRows = ArrayList<Int>()

        for (i in matrixText.indices) {
            if (!matrixText[i].contains(GALAXY)) emptyRows.add(i)
        }


        val emptyCols = ArrayList<Int>()
        for (i in 0 until  matrixText.first().length) {
            val col = matrixText.map { it[i] }
            if (!col.contains(GALAXY)) emptyCols.add(i)
        }

        var expandedGalaxy = ArrayList<String>()

        for (i in matrixText.indices) {
            expandedGalaxy.add(matrixText[i])
            if (emptyRows.contains(i)) expandedGalaxy.add(matrixText[i])
        }


        val fullGalaxy = expandedGalaxy.map {
            val sb = StringBuilder()
            for (i in 0 until expandedGalaxy.first().length) {
                sb.append(it[i])
                if (emptyCols.contains((i)))
                    sb.append(".")
            }
            sb.toString()
        }

        val extendedGalaxies = ArrayList<Galaxy>()
        for (i in fullGalaxy.indices){
            for (j in 0 until fullGalaxy.first().length) {
                if (fullGalaxy[i][j] == GALAXY) {
                    extendedGalaxies.add(Galaxy(i, j))
                }
            }
        }

        println(extendedGalaxies)
        val calculateGalaxyPair = calculateGalaxyPair(extendedGalaxies)
        val sum = calculateGalaxyPair.sumOf { calculateSum(it) }
        return sum
    }


    fun calculateFarthestDistancesSum(filename: String) : Long{
        val resource = this.javaClass.classLoader.getResource(filename)
        val matrixText = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val galaxies = ArrayList<Galaxy>()

        val maxX = matrixText.first().length
        val maxY = matrixText.size

        for (i in 0 until maxY){
            for (j in 0 until maxX) {
                if (matrixText[j][i] == GALAXY) {
                    galaxies.add(Galaxy(i, j))
                }
            }
        }

        println(galaxies)

        val emptyRows = ArrayList<Int>()
        for (i in matrixText.indices) {
            if (!matrixText[i].contains(GALAXY)) emptyRows.add(i)
        }

        val emptyCols = ArrayList<Int>()
        for (i in 0 until  matrixText.first().length) {
            val col = matrixText.map { it[i] }
            if (!col.contains(GALAXY)) emptyCols.add(i)
        }

        var expandedGalaxy = ArrayList<String>()

        for (i in matrixText.indices) {
            expandedGalaxy.add(matrixText[i])
            val newLine = matrixText[i].replace(".", "*")
            if (emptyRows.contains(i)) expandedGalaxy.add(newLine)
        }


        val fullGalaxy = expandedGalaxy.map {
            val sb = StringBuilder()
            for (i in 0 until expandedGalaxy.first().length) {
                sb.append(it[i])
                if (emptyCols.contains((i)))
                    sb.append("*")
            }
            sb.toString()
        }

        val extendedGalaxies = ArrayList<Galaxy>()
        for (i in fullGalaxy.indices){
            for (j in 0 until fullGalaxy.first().length) {
                if (fullGalaxy[i][j] == GALAXY) {
                    extendedGalaxies.add(Galaxy(j, i))
                }
            }
        }

        for (i in fullGalaxy.indices) {
            for (j in 0 until fullGalaxy.first().length) {
                print(fullGalaxy[i][j])
            }
            println()
        }

        println(extendedGalaxies)
        val calculateGalaxyPair = calculateGalaxyPair(extendedGalaxies)
        val sum = calculateGalaxyPair.sumOf { calculateOldestSum(it, fullGalaxy) }
        return sum
    }

    private fun calculateGalaxyPair(galaxies: List<Galaxy>) : List<Pair<Galaxy, Galaxy>>{
        val pairs = ArrayList<Pair<Galaxy, Galaxy>>()

        for (i in galaxies.indices){
            for (j in i + 1 until galaxies.size){
                val pair  = Pair(galaxies[i], galaxies[j])
                pairs.add(pair)
            }
        }
        return pairs
    }

    private fun calculateSum(pair: Pair<Galaxy, Galaxy>) : Long{
        val yDiff = Math.abs(pair.second.y - pair.first.y)
        val xDiff = Math.abs(pair.second.x - pair.first.x)
        val returnedLong = (xDiff + yDiff).toLong()
        return returnedLong
    }

    private fun calculateOldestSum(pair: Pair<Galaxy, Galaxy>, galaxy: List<String>) : Long{
        val minX = Math.min(pair.first.x, pair.second.x)
        val maxX = Math.max(pair.first.x, pair.second.x)

        val minY = Math.min(pair.first.y, pair.second.y)
        val maxY = Math.max(pair.first.y, pair.second.y)

        val crossingX = galaxy.first().substring(minX, maxX).count{it == '*'}
        val crossingY = galaxy.map { it.first() }.subList(minY, maxY).count { it == '*' }

        val multiply = 1000000 - 1

        val yDiff = Math.abs(pair.second.y - pair.first.y) - crossingY + multiply*crossingY
        val xDiff = Math.abs(pair.second.x - pair.first.x) - crossingX + multiply*crossingX
        val returnedLong = (xDiff + yDiff).toLong()

//        println("${pair.first.x}, ${pair.first.y} to ${pair.second.x}, ${pair.second.y}: value = $xDiff + $yDiff")
        return returnedLong
    }

}