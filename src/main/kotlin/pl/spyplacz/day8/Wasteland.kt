package pl.spyplacz.day8

import java.nio.file.Paths
import kotlin.math.min

class Wasteland {

    companion object {

        class Direction(val direction: Char)

        class Node(val root: String, val left: String, val right: String){

            val nodeMap = mapOf(root to Pair(left,right))

            override fun toString(): String {
                return "$root = ($left, $right)"
            }
        }
    }

    fun calculateSteps(filename: String): Int {
        val resource = this.javaClass.classLoader.getResource(filename)
        val textLines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val nodesText = textLines.subList(2, textLines.size)
        val directions = textLines.first().map { Direction(it) }
        val nodes = nodesText.map { node ->
            val split = node.split("=")
            val root = split.first().replace(" ", "")
            val nodes = split.last().replace("(", "").replace(")", "").split(",")
            val left = nodes.first().replace(" ", "")
            val right = nodes[1].replace(" ", "")
            Node(root, left, right)
        }

        var currentNode = nodes.find { it.root == "AAA" }
        var steps = 0
        var i = 0
        while (currentNode!!.root != "ZZZ") {
            steps += 1
            val currentDirection = directions[i]
            currentNode = if (currentDirection.direction == 'R') nodes.find { it.root == currentNode!!.right } else nodes.find { it.root == currentNode!!.left }
            if (currentNode!!.root == "ZZZ") return steps
            if (i == directions.size - 1) i = 0 else i += 1
        }

        return 0
    }

    fun calculateSandstormSteps(filename: String): Long {
        val resource = this.javaClass.classLoader.getResource(filename)
        val textLines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val nodesText = textLines.subList(2, textLines.size)
        val directions = textLines.first().map { Direction(it) }
        val nodes = nodesText.map { node ->
            val split = node.split("=")
            val root = split.first().replace(" ", "")
            val nodes = split.last().replace("(", "").replace(")", "").split(",")
            val left = nodes.first().replace(" ", "")
            val right = nodes[1].replace(" ", "")
            Node(root, left, right)
        }

        val usingNodes = nodes.filter { it.root.endsWith("A") }

        println(usingNodes)
        val minFoundPaths = usingNodes.map { node ->
            calculateNodeMin(node, directions, nodes)
        }.map { it.toLong() }
        return findLCMOfListOfNumbers(minFoundPaths)
    }

    private fun calculateNodeMin(node: Node, directions: List<Direction>, nodes: List<Node>): Int{
        var steps = 0
        var i = 0
        var currentNode = node
        while (!currentNode!!.root.endsWith("Z")) {
            steps += 1
            val currentDirection = directions[i]
            currentNode = if (currentDirection.direction == 'R') nodes.find { it.root == currentNode!!.right }!! else nodes.find { it.root == currentNode!!.left }!!
            if (currentNode!!.root == "ZZZ") return steps
            if (i == directions.size - 1) i = 0 else i += 1
        }
        return steps;
    }


    fun findLCM(a: Long, b: Long): Long {
        val larger = if (a > b) a else b
        val maxLcm = a * b
        var lcm = larger
        while (lcm <= maxLcm) {
            if (lcm % a == 0L && lcm % b == 0L) {
                return lcm
            }
            lcm += larger
        }
        return maxLcm
    }

    fun findLCMOfListOfNumbers(numbers: List<Long>): Long {
        var result = numbers[0]
        for (i in 1 until numbers.size) {
            result = findLCM(result, numbers[i])
        }
        return result
    }

}