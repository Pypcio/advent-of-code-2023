package pl.spyplacz

import pl.spyplacz.day1.Trebuchet
import pl.spyplacz.day10.Pipe
import pl.spyplacz.day11.Galaxies
import pl.spyplacz.day12.Lava
import pl.spyplacz.day13.Mirrors
import pl.spyplacz.day14.Dishes
import pl.spyplacz.day15.Library
import pl.spyplacz.day2.CubeConundrum
import pl.spyplacz.day3.Engine
import pl.spyplacz.day4.Scratchcards
import pl.spyplacz.day5.Fertilizer
import pl.spyplacz.day6.Race
import pl.spyplacz.day7.Camel
import pl.spyplacz.day8.Wasteland
import pl.spyplacz.day9.Mirage

fun main(args: Array<String>) {
    println("Hello Advent of Code!")

//    println("Day 1")
//    val trebuchet = Trebuchet()
//    val trebuchetCalibration1 = trebuchet.calculateCalibration("trebuchet.txt")
//    println("Trebuchet: $trebuchetCalibration1")
//    val trebuchetCalibration2 = trebuchet.calculateCalibrationWithWordParsing("trebuchet.txt")
//    println("Trebuchet with word parsing: $trebuchetCalibration2")
//
//    println("Day 2")
//    val conundrum = CubeConundrum()
//    val cubeConundrum = conundrum.calculateConundrum("conundrum.txt", 12,13,14)
//    println("Cube conundrum: $cubeConundrum")
//    val power = conundrum.calculatePower("conundrum.txt")
//    println("Cube power: $power")
//
//    println("Day 3")
//    val engine = Engine()
//    val schematic = engine.engineSchematic("engine.txt")
//    println("engine schematic: $schematic")
//    val gearRatios = engine.gearRatios("engine.txt")
//    println(gearRatios)
//
//    println("Day 4")
//    val scratchcards = Scratchcards()
//    val points = scratchcards.calculatePoints("scratchcards.txt")
//    println("scratchcards points: $points")
//    val scratches = scratchcards.calculateScratchcards("scratchcards.txt")
//    println("scratchcards: $scratches")
//
//    println("Day 5")
//    val fertilizer = Fertilizer()
//    val lowestLocation = fertilizer.getLowestLocation("fertilizer.txt")
//    println("lowest location: $lowestLocation")
//    val lowestLocation2 = fertilizer.getLowestLocation2("fertilizer.txt")
//    println("lowest location extended: $lowestLocation2")
//
//    println("Day 6")
//    val race = Race()
//    val raceRecords = race.waysBeatingRecords("race.txt")
//    println("ways to beat record: $raceRecords")
//    val singleRaceRecords = race.waysBeatingRace("race.txt")
//    println("single run records: $singleRaceRecords")
//
//    println("Day 7")
//    val camel = Camel()
//    val cardWinnings = camel.calculateTotalWinnings("camel.txt")
//    println("Card winnings: $cardWinnings")
//    val cardJokerWinnings = camel.calculateTotalJokerWinnings("camel.txt")
//    println("Joker card winnings $cardJokerWinnings")
//
//    println("Day 8")
//    val wasteland = Wasteland()
//    val steps = wasteland.calculateSteps("wasteland.txt")
//    println("Steps: $steps")
//    val sandstormSteps = wasteland.calculateSandstormSteps("wasteland.txt")
//    println("Sandstorm steps: $sandstormSteps")
//
//    println("Day 9")
//    val mirage = Mirage()
//    val history = mirage.sumExtrapolatedValues("mirage.txt")
//    println("History mirage: $history")
//    val backwardsHistory = mirage.sumBackwardsExtrapolatedValues("mirage.txt")
//    println("Backwards history mirage: $backwardsHistory")

//    println("Day 10")
//    val pipe = Pipe()
//    val farthestPosition = pipe.calculateSteps("steps.txt")
//    println("farthestPosition: $farthestPosition")
//    val titesInside = pipe.calculateEnclosed("steps.txt")
//    println("tites Inside: $titesInside")

//    println("Day 11")
//    val galaxies = Galaxies()
//    val sum = galaxies.calculateDistancesSum("galaxy.txt")
//    println("distance sum: $sum")
//    val sum2 = galaxies.calculateFarthestDistancesSum("galaxy.txt")
//    println("distance sum: $sum2")

//    println("Day 12")
//    val lava = Lava()
//    val sum = lava.calculateFoldAvailableOptions("lava.txt")
//    println("sum of arrangements: $sum")

//    println("Day 13")
//    val mirrors = Mirrors()
//    val sum = mirrors.calculateNotes("mirrors.txt")
//    println("sum of notes: $sum")

//    println("Day 14")
//    val dishes = Dishes()
//    val sum = dishes.calculateNorthSupportBeam("dishes.txt")
//    println("sum of dishes: $sum")

    println("Day 15")
    val library = Library()
    val sum = library.hash("library.txt")
    println("sum of hashes: $sum")
    val sumPower = library.hashPower("library.txt")
    println("sum of hashes power: $sumPower")
}