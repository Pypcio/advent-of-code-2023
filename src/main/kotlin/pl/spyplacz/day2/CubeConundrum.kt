package pl.spyplacz.day2

import pl.spyplacz.main
import java.nio.file.Paths

class CubeConundrum {

    companion object {
        class Bag(val red: Int, val green: Int, val blue: Int)

        class Game(val number: Int, val bags: List<Bag>) {

            fun validate(red: Int, green: Int, blue: Int) : Boolean {
                val redFilter = bags.map { x -> x.red }.max() <= red
                val greenFilter = bags.map { x -> x.green }.max() <= green
                val blueFilter = bags.map { x -> x.blue }.max() <= blue
                return redFilter && greenFilter && blueFilter
            }

            fun power() : Int {
                val redMin = bags.map { x -> x.red }.max()
                val greenMin = bags.map { x -> x.green }.max()
                val blueMin = bags.map { x -> x.blue }.max()
                val power = redMin * greenMin * blueMin
                return power
            }
        }
    }

    fun calculateConundrum(fileName: String, red: Int, green: Int, blue: Int): Int {
        val resource = this.javaClass.classLoader.getResource(fileName)
        val lines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val games = lines.map { lineToGame(it) }
        val filteredGames = games.filter { x -> x.validate(red, green, blue) }
        val conundrum = filteredGames.sumOf { it.number }
        return conundrum
    }

    fun calculatePower(fileName: String): Int {
        val resource = this.javaClass.classLoader.getResource(fileName)
        val lines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val games = lines.map { lineToGame(it) }
        val powers = games.map { it.power() }
        return powers.sum()
    }


    private fun lineToGame(line: String): Game{
        val split = line.split(":")
        val number = split.first().split(" ").last().toInt()
        val bagLines = split.last().split(";")
        val bagSpecificList = bagLines.map { x -> x.split(",") }
        val bagMap = bagSpecificList.map {bagSpecific ->
            bagSpecific.map { x -> x.split(" ")}
                .map { x -> mapOf(x[2] to x[1]) }
                .flatMap { it.entries }
                .associate { it.key to it.value.toInt() }
        }
        val bags = bagMap.map { buildBag(it) }
        val game = Game(number, bags)
        return game
    }

    private fun buildBag(cubeMap: Map<String, Int>): Bag {
        val red = cubeMap.getOrDefault("red", 0)
        val green = cubeMap.getOrDefault("green", 0)
        val blue = cubeMap.getOrDefault("blue", 0)
        return Bag(red, green, blue)
    }
}