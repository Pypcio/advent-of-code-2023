package pl.spyplacz.day4

import java.nio.file.Paths
import kotlin.math.pow

class Scratchcards {

    companion object {

        class Card(val number: Int, val myNumbers: List<Int>, val winningNumbers: List<Int>) {

            fun calculateMatchedNumbers() : Int {
                return winningNumbers.filter { myNumbers.contains(it) }.size
            }

            fun calculatePoint(): Int {
                val filteredWinningNumbers = calculateMatchedNumbers()
                val points = 2.0.pow((filteredWinningNumbers - 1).toDouble()).toInt()
                println("Card $number: matched numbers: $filteredWinningNumbers, point(s): $points")
                return points
            }
        }

    }

    fun calculatePoints(fileName: String): Int {
        val cards = parseFile(fileName)
        println("cards: ${cards}")
        val cardsPoints = cards.map { it.calculatePoint() }
        return cardsPoints.sum()
    }


    fun calculateScratchcards(fileName: String): Int {
        val cards = parseFile(fileName)
        println("cards: $cards")
        val numberOfCards : MutableMap<Int, Int> = cards
            .map { mutableMapOf(it.number to 1) }
            .flatMap { it.entries }
            .associate { it.key to it.value }
            .toMutableMap()
        val cardMatches = cards.map { mapOf(it.number to it.calculateMatchedNumbers()) }
            .flatMap { it.entries }
            .associate { it.key to it.value }

        println("cards: $cardMatches")

        for (i in 1..cards.size) {
            val currentCards = numberOfCards[i]!!
            val currentCardMatches = cardMatches.getOrDefault(i, 0)

            for (j in i+1 until i+1+currentCardMatches) {
                if (numberOfCards.containsKey(j)) {
                    val newCardNumbers = numberOfCards[j]!! + currentCards
                    numberOfCards[j] = newCardNumbers
                }
            }

        }

        println("cards after check winnings: $numberOfCards")
        val sumOfCards = numberOfCards.map { it.value }.sum()
        return sumOfCards
    }

    private fun parseFile(filename:String): List<Card> {
        val resource = this.javaClass.classLoader.getResource(filename)
        val cardLines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val cards = cardLines.map { buildCard(it) }
        return cards
    }

    private fun buildCard(line: String): Card {
        val cardNumberSplit = line.split(":")
        val number = cardNumberSplit.first().split(" ").last().toInt()

        val numberSplit = cardNumberSplit.last().split("|")
        val myNumbers = numberSplit.first().trim().split(" ").filter { it.isNotEmpty() }.map { it.toInt() }
        val winningNumbers = numberSplit.last().trim().split(" ").filter { it.isNotEmpty() }.map { it.toInt() }

        return Card(number, myNumbers, winningNumbers)
    }

}