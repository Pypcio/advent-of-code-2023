package pl.spyplacz.day7

import java.nio.file.Paths
import kotlin.streams.toList

class Camel {

    companion object {

        val cardValueMap = mapOf(
                'A' to 14,
                'K' to 13,
                'Q' to 12,
                'J' to 11,
                'T' to 10,
                '9' to 9,
                '8' to 8,
                '7' to 7,
                '6' to 6,
                '5' to 5,
                '4' to 4,
                '3' to 3,
                '2' to 2
        )

        val cardJokerValueMap = mapOf(
                'A' to 14,
                'K' to 13,
                'Q' to 12,
                'T' to 10,
                '9' to 9,
                '8' to 8,
                '7' to 7,
                '6' to 6,
                '5' to 5,
                '4' to 4,
                '3' to 3,
                '2' to 2,
                'J' to 1,

                )


        class Card(val cardLine: String) {

            val cardList = cardLine.map { cardValueMap[it] }
            val uniqueSymbols = cardLine.toCharArray().toSet()
            val pairs = uniqueSymbols.filter { ch -> cardLine.count{it == ch } == 2 }.size
            val threes = uniqueSymbols.filter { ch -> cardLine.count{it == ch } == 3 }.size
            val fours = uniqueSymbols.filter { ch -> cardLine.count{it == ch } == 4 }.size
            val fives = uniqueSymbols.filter { ch -> cardLine.count{it == ch } == 5 }.size
            val cardValues = cardList.map { it!! }
            fun isTwoPairs() = pairs == 2
            fun isOnePair() = pairs == 1
            fun isThreeOfKind() = threes == 1
            fun isFullHouse() = threes == 1 && pairs == 1
            fun isFourOfKind() = fours == 1
            fun isFiveOfKind() = fives == 1


            override fun toString(): String {
                return "$cardLine, fives: $fives, fours: $fours, threes: $threes, pairs: $pairs"
            }
        }

        class JokerCard(val cardLine: String) {

            val cardList = cardLine.map { cardJokerValueMap[it] }
            val uniqueSymbols = cardLine.toCharArray().toSet()
            private val fives = uniqueSymbols.filter { ch -> cardLine.count{it == ch && ch != 'J' } == 5}.size
            private val fours = uniqueSymbols.filter { ch -> cardLine.count{it == ch && ch != 'J'} == 4 }.size
            private val threes = uniqueSymbols.filter { ch -> cardLine.count{it == ch && ch != 'J'} == 3 }.size
            private val pairs = uniqueSymbols.filter { ch -> cardLine.count{it == ch && ch != 'J'} == 2 }.size
            val jokers = cardLine.count{it == 'J' }
            val cardValues = cardList.map { it!! }
            val JFiveOfKind = fives == 1 || fours == 1 && jokers == 1 || threes == 1 && jokers == 2 || pairs == 1 && jokers == 3 || jokers == 4 || jokers == 5
            val JFourOfKind = !JFiveOfKind && fours == 1 || threes == 1 && jokers == 1 || pairs == 1 && jokers == 2 || jokers == 3
            val JFullHouse = !JFourOfKind && !JFourOfKind && threes == 1 && pairs == 1 || pairs == 2 && jokers == 1
            val JThreeOfKind = !JFourOfKind && !JFourOfKind && !JFullHouse && threes == 1 || pairs == 1 && jokers == 1 || jokers == 2
            val JisTwoPairs = !JFourOfKind && !JFourOfKind && !JFullHouse && pairs == 2
            val JOnePair = !JFourOfKind && !JFourOfKind && !JFullHouse && !JisTwoPairs && pairs == 1 || jokers == 1

            override fun toString(): String {
                return "$cardLine, fives: $JFiveOfKind, fours: $JFourOfKind, fullHouse: ${JFullHouse}, threes: $JThreeOfKind, pairs: $JisTwoPairs"
            }
        }

    }
    fun calculateTotalWinnings(filename: String) : Int {
        val resource = this.javaClass.classLoader.getResource(filename)
        val textLines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val cardMap = textLines.map { line ->
            Pair(Card(line.split(" ").first()), line.split(" ").last().toInt())

        }
        val sortedBy = cardMap.sortedWith(
                compareByDescending<Pair<Card, Int>> { it.first.fives }
                        .thenByDescending { it.first.fours }
                        .thenByDescending { it.first.isFullHouse() }
                        .thenByDescending { it.first.threes }
                        .thenByDescending { it.first.pairs }
                        .thenByDescending { it.first.cardValues[0] }
                        .thenByDescending { it.first.cardValues[1] }
                        .thenByDescending { it.first.cardValues[2] }
                        .thenByDescending { it.first.cardValues[3] }
                        .thenByDescending { it.first.cardValues[4] }
        )

        var winningValue = 0
        val fromLowestList = sortedBy.map { it.second }.reversed()
        for (i in sortedBy.indices){
            winningValue += (i+1)*fromLowestList[i]
        }
        return winningValue
    }

    fun calculateTotalJokerWinnings(filename: String) : Int {
        val resource = this.javaClass.classLoader.getResource(filename)
        val textLines = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val cardMap = textLines.map { line ->
            Pair(JokerCard(line.split(" ").first()), line.split(" ").last().toInt())

        }
        val fives = cardMap.filter { it.first.JFiveOfKind }
                .sortedWith(
                        compareByDescending<Pair<JokerCard, Int>> { it.first.cardValues[0] }
                                .thenByDescending { it.first.cardValues[1] }
                                .thenByDescending { it.first.cardValues[2] }
                                .thenByDescending { it.first.cardValues[3] }
                                .thenByDescending { it.first.cardValues[4] }
                )
        val fours = cardMap.filter { it.first.JFourOfKind && !it.first.JFiveOfKind }
                .sortedWith(
                        compareByDescending<Pair<JokerCard, Int>> { it.first.cardValues[0] }
                                .thenByDescending { it.first.cardValues[1] }
                                .thenByDescending { it.first.cardValues[2] }
                                .thenByDescending { it.first.cardValues[3] }
                                .thenByDescending { it.first.cardValues[4] }
                )
        val houses = cardMap.filter { it.first.JFullHouse && !it.first.JFourOfKind}
                .sortedWith(
                        compareByDescending<Pair<JokerCard, Int>> { it.first.cardValues[0] }
                                .thenByDescending { it.first.cardValues[1] }
                                .thenByDescending { it.first.cardValues[2] }
                                .thenByDescending { it.first.cardValues[3] }
                                .thenByDescending { it.first.cardValues[4] }
                )
        val threes = cardMap.filter { it.first.JThreeOfKind }
                .sortedWith(
                        compareByDescending<Pair<JokerCard, Int>> { it.first.cardValues[0] }
                                .thenByDescending { it.first.cardValues[1] }
                                .thenByDescending { it.first.cardValues[2] }
                                .thenByDescending { it.first.cardValues[3] }
                                .thenByDescending { it.first.cardValues[4] }
                )
        val twoPairs = cardMap.filter { it.first.JisTwoPairs }
                .sortedWith(
                        compareByDescending<Pair<JokerCard, Int>> { it.first.cardValues[0] }
                                .thenByDescending { it.first.cardValues[1] }
                                .thenByDescending { it.first.cardValues[2] }
                                .thenByDescending { it.first.cardValues[3] }
                                .thenByDescending { it.first.cardValues[4] }
                )
        val pair = cardMap.filter { it.first.JOnePair && !it.first.JFiveOfKind && !it.first.JFourOfKind && !it.first.JFullHouse && !it.first.JisTwoPairs}
                .sortedWith(
                        compareByDescending<Pair<JokerCard, Int>> { it.first.cardValues[0] }
                                .thenByDescending { it.first.cardValues[1] }
                                .thenByDescending { it.first.cardValues[2] }
                                .thenByDescending { it.first.cardValues[3] }
                                .thenByDescending { it.first.cardValues[4] }
                )
        val highCard = cardMap.filter { !it.first.JOnePair && !it.first.JisTwoPairs && !it.first.JisTwoPairs
                && !it.first.JThreeOfKind && !it.first.JFullHouse && !it.first.JFiveOfKind && !it.first.JFourOfKind }
                .sortedWith(
                        compareByDescending<Pair<JokerCard, Int>> { it.first.cardValues[0] }
                                .thenByDescending { it.first.cardValues[1] }
                                .thenByDescending { it.first.cardValues[2] }
                                .thenByDescending { it.first.cardValues[3] }
                                .thenByDescending { it.first.cardValues[4] }
                )
        var winningValue = 0
        var sortedBy = (fives + fours + houses + threes + twoPairs + pair + highCard).distinct().reversed()
        for (i in sortedBy.indices){
            winningValue += (i+1)*sortedBy[i].second
        }
        return winningValue
    }
}