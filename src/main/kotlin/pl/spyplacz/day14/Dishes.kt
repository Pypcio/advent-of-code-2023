package pl.spyplacz.day14

import java.nio.file.Paths
import kotlin.collections.ArrayList

class Dishes {

    companion object {

        class Matrix(val textMatrix: List<String>) {
            val cols = ArrayList<String>()
            fun fillRows(){
                for (i in 0 until textMatrix.first().length) {
                    val row = String(textMatrix.map { it[i] }.toCharArray())
                    cols.add(row)
                }
            }

            fun moveRocksForTheNorth(){
                for (i in cols.indices){
                    val stopsPositions = ArrayList<Int>()
                    var rockPositions = ArrayList<Int>()
                    val rowLength = cols[i].length
                    for (j in 0 until rowLength){
                        val char = cols[i][j]
                        if (char == 'O') rockPositions.add(j)
                        else if (char == '#') stopsPositions.add(j)
                    }

                    val sb = StringBuilder()

                    for (i in 0 until rowLength) {
                        if (stopsPositions.isNotEmpty() && rockPositions.isNotEmpty()) {
                            if (stopsPositions.first() == i) {
                                sb.append("#")
                                stopsPositions.removeFirst()
                            } else if (stopsPositions.first() > rockPositions.first()) {
                                sb.append("0")
                                rockPositions.removeFirst()
                            } else if (stopsPositions.first() < rockPositions.first()) {
                                sb.append(".")
                            } else sb.append(".")
                        } else if (rockPositions.isNotEmpty()) {
                            sb.append("0")
                            rockPositions.removeFirst()
                        } else if (stopsPositions.isNotEmpty()) {
                            if (stopsPositions.first() == i) {
                                sb.append("#")
                                stopsPositions.removeFirst()
                            } else {
                                sb.append(".")
                            }
                        } else {
                            sb.append(".")
                        }
                    }

                    cols[i] = sb.toString()
                }
            }

            fun calculate(): Long {
                val rows = ArrayList<String>()

                    for (i in 0 until cols.first().length) {
                        val row = String(cols.map { it[i] }.toCharArray())
                        rows.add(row)
                    }

                var sum = 0L
                var factor = rows.size
                for (i in rows.indices) {
                    val count = rows[i].count{it == '0'}
                    sum += count*factor
                    factor -= 1
                }
                return sum
            }

        }

    }
    fun calculateNorthSupportBeam(filename: String): Long{
        val resource = this.javaClass.classLoader.getResource(filename)
        val matrixesText = Paths.get(resource!!.toURI()).toFile().useLines { it.toList() }
        val matrix = Matrix(matrixesText)
        matrix.fillRows()
        matrix.moveRocksForTheNorth()
        val sum = matrix.calculate()
        return sum
    }
}