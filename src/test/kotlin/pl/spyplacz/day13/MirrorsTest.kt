package pl.spyplacz.day13

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class MirrorsTest {

    val mirrors = Mirrors()

    @Test
    fun getNotes(){
        val calculateNotes = mirrors.calculateNotes("day13Example.txt")
        Assertions.assertEquals(405, calculateNotes)
    }
}