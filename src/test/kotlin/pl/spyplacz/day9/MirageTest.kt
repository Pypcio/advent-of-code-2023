package pl.spyplacz.day9

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class MirageTest {

    private val mirage = Mirage()

    @Test
    fun testMirage() {
        val sum = mirage.sumExtrapolatedValues("day9Example.txt")
        Assertions.assertEquals(114, sum)
    }

    @Test
    fun testBackwardsMirage() {
        val sum = mirage.sumBackwardsExtrapolatedValues("day9Example.txt")
        Assertions.assertEquals(2, sum)
    }
}