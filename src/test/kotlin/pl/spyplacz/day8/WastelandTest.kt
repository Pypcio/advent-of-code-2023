package pl.spyplacz.day8

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class WastelandTest {

    val wasteland = Wasteland()

//    @Test
//    fun calculateSteps(){
//        val calculateSteps = wasteland.calculateSteps("day8Example.txt")
//        Assertions.assertEquals(2, calculateSteps)
//    }

//    @Test
//    fun calculateStep2s(){
//        val calculateSteps = wasteland.calculateSteps("day8Example2.txt")
//        Assertions.assertEquals(6, calculateSteps)
//    }
    @Test
    fun calculateSteps3(){
        val calculateSteps = wasteland.calculateSandstormSteps("day8Example3.txt")
        Assertions.assertEquals(6, calculateSteps)
    }
}