package pl.spyplacz.day11

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class GalaxyTest {

    val galaxies = Galaxies()

    @Test
    fun  calculateDistance(){
        val calculateDistance = galaxies.calculateDistancesSum("day11Example.txt")
        Assertions.assertEquals(374, calculateDistance)
    }

    @Test
    fun  calculateDistance2(){
        val calculateDistance = galaxies.calculateFarthestDistancesSum("day11Example.txt")
        Assertions.assertEquals(8410, calculateDistance)
    }
}