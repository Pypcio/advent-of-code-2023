package pl.spyplacz.day6

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class RaceTest {

    val race = Race()

//    @Test
//    fun testRaceRecords() {
//        val raceRecords = race.waysBeatingRecords("day6Example.txt")
//        Assertions.assertEquals(288, raceRecords)
//    }

    @Test
    fun testSingleRaceRecords() {
        val raceRecords = race.waysBeatingRace("day6Example.txt")
        Assertions.assertEquals(71503, raceRecords)
    }
}