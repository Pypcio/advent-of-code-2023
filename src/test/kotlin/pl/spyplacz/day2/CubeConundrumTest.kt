package pl.spyplacz.day2

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CubeConundrumTest {

//    @Test
//    fun testConundrum(){
//        val cubeConundrum = CubeConundrum()
//        val calculateConundrum = cubeConundrum.calculateConundrum("day2Example.txt", 12, 13, 14)
//        Assertions.assertEquals(8, calculateConundrum)
//    }

    @Test
    fun testPower(){
        val cubeConundrum = CubeConundrum()
        val calculateConundrum = cubeConundrum.calculatePower("day2Example.txt")
        Assertions.assertEquals(2286, calculateConundrum)
    }
}