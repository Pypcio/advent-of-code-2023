package pl.spyplacz.day1

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import pl.spyplacz.day1.Trebuchet

class TrebuchetTest {

    @Test
    fun testTrebuchet() {
        println("--- Day 1: Trebuchet?! --- ")
        val day1 = Trebuchet()
        val calculateCalibration = day1.calculateCalibration("day1Example.txt")
        val result = "--- Result:  $calculateCalibration --- "
        println(result)
        Assertions.assertEquals(142, calculateCalibration)
    }

    @Test
    fun testTrebuchetExtended() {
        println("--- Day 1: Trebuchet?! --- ")
        val day1 = Trebuchet()
        val calculateCalibration = day1.calculateCalibration("day1Example2.txt")
        val result = "--- Result:  $calculateCalibration --- "
        println(result)
        Assertions.assertEquals(281, calculateCalibration)
    }

    @Test
    fun testTrebuchetExtended2() {
        println("--- Day 1: Trebuchet?! --- ")
        val day1 = Trebuchet()
        val calculateCalibration = day1.calculateCalibration("day1Example3.txt")
        val result = "--- Result:  $calculateCalibration --- "
        println(result)
        Assertions.assertEquals(31, calculateCalibration)
    }

    @Test
    fun testTrebuchetFinal() {
        println("--- Day 1: Trebuchet?! --- ")
        val day1 = Trebuchet()
        val calculateCalibration = day1.calculateCalibration("trebuchet.txt")
        val result = "--- Result:  $calculateCalibration --- "
        println(result)
        Assertions.assertEquals(53348, calculateCalibration)
    }

}