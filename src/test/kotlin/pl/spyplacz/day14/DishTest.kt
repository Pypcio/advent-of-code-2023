package pl.spyplacz.day14

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DishTest {

    val dishes = Dishes()

    @Test
    fun getNotes(){
        val northSupportBeam = dishes.calculateNorthSupportBeam("day14Example.txt")
        Assertions.assertEquals(136, northSupportBeam)
    }
}