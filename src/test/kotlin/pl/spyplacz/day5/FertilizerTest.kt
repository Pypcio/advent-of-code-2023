package pl.spyplacz.day5

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class FertilizerTest {

    val fertilizer = Fertilizer()

//    @Test
//    fun testFertilizer(){
//        val lowestLocation = fertilizer.getLowestLocation("day5Example.txt")
//        Assertions.assertEquals(35, lowestLocation)
//    }

    @Test
    fun testFertilizerExtended(){
        val lowestLocation = fertilizer.getLowestLocation2("day5Example.txt")
        Assertions.assertEquals(46, lowestLocation)
    }
}