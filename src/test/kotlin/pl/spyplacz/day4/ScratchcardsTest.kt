package pl.spyplacz.day4

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ScratchcardsTest {

    private val scratchcards = Scratchcards()

//    @Test
//    fun testCalculatePoints(){
//        val calculatePoints = scratchcards.calculatePoints("day4Example.txt")
//        Assertions.assertEquals(13, calculatePoints)
//    }

    @Test
    fun testCalculateScratchcards(){
        val calculatePoints = scratchcards.calculateScratchcards("day4Example.txt")
        Assertions.assertEquals(30, calculatePoints)
    }

}