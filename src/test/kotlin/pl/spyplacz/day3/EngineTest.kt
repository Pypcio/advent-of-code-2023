package pl.spyplacz.day3

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class EngineTest {

    private val engine = Engine()

    @Test
    fun testEngine(){
        val engineSchematic = engine.engineSchematic("day3Example.txt")
        Assertions.assertEquals(4361, engineSchematic)
    }

    @Test
    fun testGears(){
        val gearRatios = engine.gearRatios("day3Example.txt")
        Assertions.assertEquals(467835, gearRatios)
    }

}