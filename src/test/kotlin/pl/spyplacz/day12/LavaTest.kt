package pl.spyplacz.day12

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class LavaTest {

    val lava = Lava()

//    @Test
//    fun calculateOptions(){
//        val calculateAvailableOptions = lava.calculateAvailableOptions("day12Example.txt")
//        Assertions.assertEquals(21, calculateAvailableOptions)
//    }

    @Test
    fun calculateFoldOptions(){
        val calculateAvailableOptions = lava.calculateFoldAvailableOptions("day12Example.txt")
        Assertions.assertEquals(525152, calculateAvailableOptions)
    }
}