package pl.spyplacz.day15

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class LibraryTest {

    val library = Library()

    @Test
    fun testHash(){
        val hash = library.hash("day15Example.txt")
        Assertions.assertEquals(52, hash)
    }

    @Test
    fun testHash2(){
        val hash = library.hash("day15Example2.txt")
        Assertions.assertEquals(1320, hash)
    }


    @Test
    fun testHashForBoxes(){
        val hash = library.hashPower("day15Example2.txt")
        Assertions.assertEquals(145, hash)
    }
}