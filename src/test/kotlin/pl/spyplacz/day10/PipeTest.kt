package pl.spyplacz.day10

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class PipeTest {

    val pipe = Pipe()

    @Test
    fun calculatePipe(){
        val calculateSteps = pipe.calculateSteps("day10Example.txt")
        Assertions.assertEquals(4, calculateSteps)
    }

    @Test
    fun calculatePipe2(){
        val calculateSteps = pipe.calculateEnclosed("day10Example3.txt")
        Assertions.assertEquals(8, calculateSteps)
    }


    @Test
    fun calculatePipe3(){
        val enclosed = pipe.calculateEnclosed("day10Example2.txt")
        Assertions.assertEquals(10, enclosed)
    }
}