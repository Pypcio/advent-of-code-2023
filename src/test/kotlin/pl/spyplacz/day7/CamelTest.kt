package pl.spyplacz.day7

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CamelTest {

    val camel = Camel()

//    @Test
//    fun testCardWinning(){
//        val lowestLocation = camel.calculateTotalWinnings("day7Example.txt")
//        Assertions.assertEquals(6440, lowestLocation)
//    }

    @Test
    fun testCardJokerWinning(){
        val jokerWinnings = camel.calculateTotalJokerWinnings("day7Example.txt")
        Assertions.assertEquals(5905, jokerWinnings)
    }
}